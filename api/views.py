from rest_framework.response import Response
from rest_framework.decorators import api_view
from cinema.models import MoviesDates, MoviesDateTimes, Movies, MovieBookings

import api.send_message as send_message
from api.generate_code import get_code


# This method will be used to return either a list of movies offered or a single movie offered
# by passing an index that will be used to get the specific movie from the list of movies
# This index passed is the index a user selects, when prompted to choose the movie they want
def get_movie_object(index=None):
    movie = Movies.objects.values_list('id', 'name')
    if index is None:
        # returns a QuerySet e.g. [(1, 'Spiderman'), (2, 'Superman'), (3, 'Batman')]
        return movie
    else:
        if 0 < index <= len(movie):
            # returns a single tuple with the movie id and name e.g.(1, 'Spiderman')
            return movie[index - 1]
        else:
            return None


# This method will be used to return either a list of movies dates when a specific movie will be screened
# or a single movie date when a specific movie will be screened.
# By passing an index a user will get the specific movies date when a specific movie from the list
# of movies dates will be screened.
# This index passed is the index a user selects, when prompted to choose the movie date they want to book
# This movie_id is the id of the movie they earlier selected
def get_movies_dates_object(movie_id, index=None):
    movies_dates = MoviesDates.objects.filter(
        movie_id=movie_id).values_list('pk', 'dates', 'movie__name')
    if index is None:
        # returns a QuerySet e.g. [(1, '2021.03.12', 'Spiderman'), (2, '2021.04.09', 'Superman'), (3, '2021.12.12', 'Batman')]
        return movies_dates
    else:
        if 0 < index <= len(movies_dates):
            # returns a single tuple with the movies date id, the movie's screening date and movie name e.g.(1, '2021.03.12', 'Spiderman')
            return movies_dates[index - 1]
        else:
            return None


# This method will be used to return either a list of dates the movie will be screened
# or a single date the movie will be screened.
# By passing an index a user will be get the specific date the movie movies date will be traveled from the list
# of dates the movie will be screened.
# This index passed is the index a user selects, when prompted to choose the of date they want to travel the sepcified movie movies date
# This movies_dates_id is the id of the movie movies date they earlier selected.
def get_movies_date_time_object(movies_dates_id, index=None):
    movies_date_times = MoviesDateTimes.objects.filter(
        movies_dates__id=movies_dates_id).values_list('pk', 'start_time', 'end_time')
    if index is None:
        # returns a QuerySet e.g. [(1, '2021/10/13', '13:15', '15:15'), (2, '2021/10/15', '17:15', '19:15'), (3, '2021/12/13', '08:15', '10:15')]
        return movies_date_times
    else:
        if len(movies_date_times) > 0:
            # returns a single tuple with the movie id and name e.g.(1, '2021/10/13', '13:15', '15:15')
            return movies_date_times[index - 1]
        else:
            return None


# A method used to generate movies that will be displayed to a user
def get_movies():
    # This is the index that will be displayed to the user for selection
    count = 0
    response = "CON Select Movie \n"
    for _, movie in get_movie_object():
        count += 1
        response += f"{count}. {movie} \n"
    return response


# The method used to generate a list of movies dates when a specific movie will be screened
# You pass a movie_id that will be used to get movies dates when a specific movie will be screened
def get_movies_dates(movie_id):
    # This is the index that will be displayed to the user for selection
    count = 0
    movies_dates_object = get_movies_dates_object(movie_id=movie_id)
    
    if movies_dates_object is None:
        return "END No Date Available for Screening \n"

    response = "CON Select Movie Date \n"
    for _, dates, movie__name in movies_dates_object:
        count += 1
        response += f"{count}. {dates} : {movie__name} \n"
        
    if count == 0:
        response = "END No Date Available for Screening  \n"

    return response


# The method used to generate a list of times in the dates the movie will be screened.
# You pass a movies_dates_id that will be used to get dates the movie will be screened, from the movies dates time table.
def get_movies_date_time(movies_dates_id):
    # This is the index that will be displayed to the user for selection
    count = 0
    movies_date_time_object = get_movies_date_time_object(movies_dates_id=movies_dates_id)

    if movies_date_time_object is None:
        return "END No Time Available for Screening \n"
        
    response = "CON Select Time Slot to Book\n"
    for _, start_time, end_time in movies_date_time_object:
        count += 1
        response += f"{count}. {start_time}-{end_time} \n"
        

    if count == 0:
        response = "END No Time Available for Screening \n"
    return response


# The view that the U.S.S.D. will use to display data to the user
@api_view(['GET', 'POST'])
def ussd_callback(request):
    response = ""
    request = request.data
    phone_number = request.get("phoneNumber", None)
    text = request.get("text", "default")

    # The first call of the U.S.S.D. will send a blank text to the view, this will prompt us to first
    # display the movies available

    if text == '':
        response = get_movies()
    else:
        # All other calls after the first movie will not have an empty text, the text will state the index selected
        # during a particular stage or the index selected in the current stage after another index was selected from
        # a previous stage. e.g First Stage a user selects a movie, the text returned will be 1, indicating that
        # the selected the first movie that was displayed. The Second Stage a user selects a movie date,
        # the text returned will be 1*2, indicating that the selected the second movies date that was displayed after they
        # selected the first movie that was displayed

        # We are splitting the text so that we see if the user is selecting for the first time or they are selecting
        # again
        values = text.split("*")

        # If the user called the U.S.S.D. for the first time
        if len(values) == 1:
            # get the index of the movie that was selected
            selected_movie_index = int(values[0])
            # use this index to get the details of the movie that was actually selected or return None if a user
            # entered an index that was not displayed or does not exist.
            selected_movie = get_movie_object(selected_movie_index)
            selected_movie_id = -1
            if selected_movie is not None:
                # The movie returned will be a tuple with the id of the movie as the first element of the tuple
                selected_movie_id = selected_movie[0]

            # use the movie id to get the dates the movie will be screened, or return an error response if
            # the movie selected has no dates the movie will be screened.
            response = get_movies_dates(selected_movie_id)
        # If the user calls the U.S.S.D. for the second time and selects a movie's date
        # The text split will have 2 values, one for the index of the selected movie and the
        # other for the index of the selected movie's date
        elif len(values) == 2:
            # get the index of the movie that was selected
            selected_movie_index = int(values[0])
            # use this index to get the details of the movie that was actually selected or return None if a user
            # entered an index that was not displayed or a movie that  does not exist.
            selected_movie = get_movie_object(selected_movie_index)
            # The movie returned will be a tuple with the id of the movie as the first element of the tuple
            selected_movie_id = selected_movie[0]

            # get the index of the movies date that was selected
            selected_movies_date_index = int(values[1])
            # use the movie id to get the dates the movie will be screened, or return None if
            # the movie selected has no dates the movie will be screened.
            selected_movies_dates = get_movies_dates_object(movie_id=selected_movie_id,
                                                                 index=selected_movies_date_index)

            selected_movies_dates_id = -1
            if selected_movies_dates is not None:
                # The movies times on the date of screening returned will be a tuple with the id of the movies and date of movie as the first
                # element of the tuple
                selected_movies_dates_id = selected_movies_dates[0]

            # using the movies date id to get the times on the date of screening, or return an error
            # response if the movies date movie id selected has no time slotted.
            response = get_movies_date_time(selected_movies_dates_id)
        elif len(values) == 3:
            # get the index of the movie that was selected
            selected_movie_index = int(values[0])
            # use this index to get the details of the movie that was actually selected or return None if a user
            # entered an index that was not displayed or does not exist.
            selected_movie = get_movie_object(selected_movie_index)
            # The movie returned will be a tuple with the id of the movie as the first element of the tuple
            selected_movie_id = selected_movie[0]

            # get the index of the movies date that was selected
            selected_movies_date_index = int(values[1])
            # use the movie id to get the dates the movie will be screened, or return None if
            # the movie selected has no dates the movie will be screened.
            selected_movies_dates = get_movies_dates_object(movie_id=selected_movie_id,
                                                                 index=selected_movies_date_index)
            # The movie's date returned will be a tuple with the id of the movie's date as the first
            # element of the tuple
            selected_movies_dates_id = selected_movies_dates[0]

            # get the index of the date the movies date will be traveled by the selected movies date for the selected movie
            selected_movies_date_time_index = int(values[2])
            # using the movies date id to get the times on the date of screening, or return an error
            # response if the movies date movie id selected has no time slotted.
            selected_movies_date_time = get_movies_date_time_object(selected_movies_dates_id,
                                                                    index=selected_movies_date_time_index)
            # The times of screening on the date the movies will be screened returned will be a tuple with the id of the date scheduled as the first
            # element of the tuple
            selected_movies_date_time_id = selected_movies_date_time[0]

            # use the times of screening on the date scheduled id to get the specified time of date scheduled object
            movies_date_time = MoviesDateTimes.objects.get(pk=selected_movies_date_time_id)

            # invoke the get_code() method to generate and return a random booking code string
            booking_code = get_code()

            # use the user's phone number to place the booking
            appointment = MovieBookings.objects.create(movies_date_times=movies_date_time, phone_number=phone_number, booking_code = booking_code)

            if appointment is None:
                response = "END Movie Booking Failed"
            else:
                response = "END Movie Booked Successfully"
                send_message.send(phone_number, f"Movie Booked Successfully. Your booking code is {booking_code}")

    return Response(response, headers={"Content-Type": "text/plain"})
