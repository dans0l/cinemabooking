from django.urls import path

from cinema.views import (MoviesListView, MoviesCreateView, MoviesUpdateView, MoviesDeleteView,
                            MoviesDatesListView, MoviesDatesCreateView, MoviesDatesUpdateView, MoviesDatesDeleteView,
                            MoviesDateTimesListView, MoviesDateTimesCreateView, MoviesDateTimesUpdateView, MoviesDateTimesDeleteView,
                            MovieBookingsListView, MovieBookingsCreateView, MovieBookingsUpdateView, MovieBookingsDeleteView,
                            movies_image_update)

urlpatterns = [

    path('movies', MoviesListView.as_view(), name='movies-list'),
    path('movies/new/', MoviesCreateView.as_view(), name='movies-create'),
    path('movies/<int:pk>/update/', MoviesUpdateView.as_view(), name='movies-update'),
    path('movies/<int:pk>/delete/', MoviesDeleteView.as_view(), name='movies-delete'),

    path('movie_dates', MoviesDatesListView.as_view(), name='movie-dates-list'),
    path('movie_dates/new/', MoviesDatesCreateView.as_view(), name='movie-dates-create'),
    path('movie_dates/<int:pk>/update/', MoviesDatesUpdateView.as_view(), name='movie-dates-update'),
    path('movie_dates/<int:pk>/delete/', MoviesDatesDeleteView.as_view(), name='movie-dates-delete'),

    path('movie_date_times', MoviesDateTimesListView.as_view(), name='movie-date-times-list'),
    path('movie_date_times/new/', MoviesDateTimesCreateView.as_view(), name='movie-date-times-create'),
    path('movie_date_times/<int:pk>/update/', MoviesDateTimesUpdateView.as_view(), name='movie-date-times-update'),
    path('movie_date_times/<int:pk>/delete/', MoviesDateTimesDeleteView.as_view(), name='movie-date-times-delete'),

    path('movie_bookings', MovieBookingsListView.as_view(), name='movie-bookings-list'),
    path('movie_bookings/new/', MovieBookingsCreateView.as_view(), name='movie-bookings-create'),
    path('movie_bookings/<int:pk>/update/', MovieBookingsUpdateView.as_view(), name='movie-bookings-update'),
    path('movie_bookings/<int:pk>/delete/', MovieBookingsDeleteView.as_view(), name='movie-bookings-delete'),
    
    path('movie_image/<int:pk>/update/', movies_image_update, name='movie-image-update'),

]
