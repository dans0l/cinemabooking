from django.db import models
from django.utils import timezone
from PIL import Image


# Create your models here.

# This table will hold the names of the movies screened in the cinema, a brief description, url to the trailer video and a photo of the movie
class Movies(models.Model):
    # The movie's name
    name = models.CharField(max_length=100)
    # A brief description of the movie
    description = models.TextField(max_length=500, default="Enter a brief description of the movie")
    # A url trailer of the movie
    trailer_url = models.URLField(max_length=500)
    # The length of the movie
    length = models.IntegerField()

    def __str__(self):
        return f'{self.name}'

    # Override the save method
    def save(self):
        # Let the super method hanlde the save method as it would have
        super(Movies, self).save()

        # The instance of the saved movie and create a blank movie image
        movie_image = MoviesImage(movie=self)
        movie_image.save()

# This table is used to hold the promotional images of the movie
class MoviesImage(models.Model):
    # The movie
    movie = models.OneToOneField(Movies, on_delete=models.CASCADE)
    # The promotional image of the movie
    image = models.ImageField(default='default.png', upload_to='images')

    def __str__(self):
        return f'{self.movie.name} Image'

    def save(self):
        super(MoviesImage, self).save()

        img = Image.open(self.image.path)

        if img.height > 300 or img.width > 200:
            output_size = (200, 300)
            img.thumbnail(output_size)
            img.save(self.image.path)

# This table will hold the dates the movies are screened
class MoviesDates(models.Model):
    # The movie to be screened
    movie = models.ForeignKey(Movies, on_delete=models.PROTECT)
    # The date the movie will be screened
    dates = models.DateField()

    def __str__(self):
        return f'{self.movie.name} : {self.dates}'


# This table will hold the time when each or a specific movie will be screened on a particular date
class MoviesDateTimes(models.Model):
    # The date a particular movie will be screened
    movies_dates = models.ForeignKey(MoviesDates, on_delete=models.PROTECT)
    # The time the movie starts 
    start_time = models.TimeField()
    # The time the movie ends
    end_time = models.TimeField()

    def __str__(self):
        return f'{self.movies_dates.movie.name} on {self.movies_dates.dates} from {self.start_time} to {self.end_time}'


# This table will hold the movie bookings made, the phone number of the client who made the booking
class MovieBookings(models.Model):
    # The specific movie date and time booked
    movies_date_times = models.ForeignKey(MoviesDateTimes, on_delete=models.PROTECT)
    # The client's phone number
    phone_number = models.CharField(max_length=100)
    # The unique reference code generated
    booking_code = models.CharField(null=True, max_length=6)

    def __str__(self):
        return f'{self.movies_date_times}'
